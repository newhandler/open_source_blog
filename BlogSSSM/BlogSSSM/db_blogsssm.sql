/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 5.1.49-community : Database - db_blogsssm
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_blogsssm` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `db_blogsssm`;

/*Table structure for table `t_article` */

DROP TABLE IF EXISTS `t_article`;

CREATE TABLE `t_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `summary` varchar(300) DEFAULT NULL,
  `releaseDate` datetime NOT NULL,
  `clickHit` int(11) DEFAULT NULL,
  `replyHit` int(11) DEFAULT NULL,
  `typeId` int(11) DEFAULT NULL,
  `content` mediumtext,
  `keyWord` varchar(50) DEFAULT NULL,
  `publish` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `typeId` (`typeId`),
  CONSTRAINT `t_article_ibfk_1` FOREIGN KEY (`typeId`) REFERENCES `t_articletype` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_article` */

LOCK TABLES `t_article` WRITE;

UNLOCK TABLES;

/*Table structure for table `t_articlecomment` */

DROP TABLE IF EXISTS `t_articlecomment`;

CREATE TABLE `t_articlecomment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `articleId` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL,
  `commentDate` datetime DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `content` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_articlecomment` */

LOCK TABLES `t_articlecomment` WRITE;

UNLOCK TABLES;

/*Table structure for table `t_articletype` */

DROP TABLE IF EXISTS `t_articletype`;

CREATE TABLE `t_articletype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typeName` varchar(50) DEFAULT NULL,
  `orderNo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_articletype` */

LOCK TABLES `t_articletype` WRITE;

UNLOCK TABLES;

/*Table structure for table `t_link` */

DROP TABLE IF EXISTS `t_link`;

CREATE TABLE `t_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `linkUrl` varchar(100) DEFAULT NULL,
  `orderNO` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_link` */

LOCK TABLES `t_link` WRITE;

UNLOCK TABLES;

/*Table structure for table `t_user` */

DROP TABLE IF EXISTS `t_user`;

CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(20) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `profile` text,
  `nickName` varchar(20) DEFAULT NULL,
  `sign` varchar(100) DEFAULT NULL,
  `imageURL` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `t_user` */

LOCK TABLES `t_user` WRITE;

insert  into `t_user`(`id`,`userName`,`password`,`profile`,`nickName`,`sign`,`imageURL`) values (1,'pins','154b690f77a80d479289ea7570768a18','<p><br/></p><p><span style=\"font-size: 24px;\"><em></em></span></p><p><span style=\"font-size: 24px;\"><em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 背景<br/></strong></em></span></p><p><span style=\"font-size: 24px;\"><em><strong>&nbsp;&nbsp; </strong></em><span style=\"font-size: 16px;\"><img src=\"http://img.baidu.com/hi/tsj/t_0008.gif\"/>:</span><span style=\"font-size: 16px;\">作者出身寒酸，差点饿死</span></span></p><p><span style=\"font-size: 24px;\"><span style=\"font-size: 16px;\">&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"http://img.baidu.com/hi/tsj/t_0005.gif\"/>:苗条如柴，两岁半会步行</span></span></p><p><span style=\"font-size: 24px;\"><span style=\"font-size: 16px;\">&nbsp;&nbsp;&nbsp;<img src=\"http://img.baidu.com/hi/tsj/t_0036.gif\"/> :排名第二固若金汤，倒数</span></span></p><p><span style=\"font-size: 24px;\"><span style=\"font-size: 16px;\">&nbsp;&nbsp; <img src=\"http://img.baidu.com/hi/tsj/t_0029.gif\"/> :伙伴众多跨越年龄，错号</span></span></p><p><span style=\"font-size: 24px;\"><span style=\"font-size: 16px;\">&nbsp;&nbsp;&nbsp; <img src=\"http://img.baidu.com/hi/tsj/t_0019.gif\"/>:戴帽小头<span style=\"font-size: 24px;\"><span style=\"font-size: 16px;\">，</span></span>得名飞哥国树</span></span></p><p><span style=\"font-size: 24px;\"><em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br/></strong></em></span><br/></p><p><br/></p><p><span style=\"font-size: 24px;\"><em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; pins1865</strong></em></span></p><p><span style=\"font-size: 24px;\"><em><strong>飞哥(QQ:1298869583/2057712657)</strong></em></span></p><p><span style=\"font-size: 24px;\"><em><strong><br/></strong></em></span></p><p><span style=\"font-size: 24px;\"><em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 毕业于贵州盛华职业学院</strong></em></span></p><p><span style=\"font-size: 24px;\"><span style=\"font-size: 16px;\"><br/></span></span></p><p><span style=\"font-size: 16px;\">&nbsp;&nbsp;&nbsp; <em><strong><span style=\"font-size: 36px; color: rgb(255, 0, 0);\">学生时代:</span></strong></em></span></p><p>&nbsp;<img alt=\"1.jpg\" src=\"../../static/userImages/20180607/1528379508283055066.jpg\" title=\"1528379508283055066.jpg\"/> &nbsp; &nbsp; &nbsp; <img alt=\"2.jpg\" src=\"../../static/userImages/20180607/1528379519626056881.jpg\" title=\"1528379519626056881.jpg\"/><br/></p><p><img alt=\"3 (2).jpg\" src=\"../../static/userImages/20180607/1528379549064038824.jpg\" title=\"1528379549064038824.jpg\"/></p><p><strong><em>&nbsp;&nbsp;&nbsp; </em></strong><br/><span style=\"font-size: 16px;\"><em><strong><span style=\"font-size: 36px; color: rgb(255, 0, 0);\"></span></strong></em></span></p><p><span style=\"font-size: 24px;\"><span style=\"font-size: 16px;\"><br/></span></span></p><p><span style=\"font-size: 24px;\"></span>&nbsp;&nbsp; <span style=\"color: rgb(255, 0, 0);\"><strong><em><span style=\"font-size: 36px;\">毕业后:</span></em></strong></span></p><p><span style=\"color: rgb(255, 192, 0);\"><strong><em><span style=\"font-size: 36px;\">&nbsp;&nbsp;</span></em></strong><span style=\"font-size: 24px;\">虽然差点，但是还能住</span><span style=\"font-size: 20px;\"></span></span></p><p>&nbsp;&nbsp;&nbsp;</p><p><br/></p><p>&nbsp;&nbsp;&nbsp; <img alt=\"3.jpg\" src=\"../../static/userImages/20180607/1528379572829007916.jpg\" title=\"1528379572829007916.jpg\"/></p><p>&nbsp;&nbsp;&nbsp;&nbsp;<img alt=\"\" title=\"\" src=\"../../static/userImages/20180607/1528379602095068706.jpg\" width=\"389\" height=\"257\"/></p><p>&nbsp;&nbsp;&nbsp;&nbsp;<br/></p><p>&nbsp;&nbsp;&nbsp;&nbsp;<br/></p><p>&nbsp;&nbsp;&nbsp;<br/></p><p><strong><em>&nbsp;&nbsp;&nbsp; </em></strong><span style=\"color: rgb(255, 0, 0);\"><strong><em><span style=\"font-size: 36px;\">工作后:</span></em></strong></span></p><p><strong><em>&nbsp;&nbsp;&nbsp; <br/></em></strong></p><p><strong><em>&nbsp;&nbsp;&nbsp;&nbsp;<img alt=\"5.jpg\" src=\"../../static/userImages/20180607/1528379624314058211.jpg\" title=\"1528379624314058211.jpg\"/><br/></em></strong></p><p><strong><em>&nbsp;&nbsp;&nbsp;&nbsp;<img alt=\"6.jpg\" src=\"../../static/userImages/20180607/1528379635017021299.jpg\" title=\"1528379635017021299.jpg\"/></em></strong><br/></p><p><strong><em>&nbsp;&nbsp;&nbsp; <br/></em></strong></p><p><strong><em>&nbsp;&nbsp;&nbsp; <img src=\"http://img.baidu.com/hi/tsj/t_0003.gif\"/><span style=\"font-size: 24px;\">:<span style=\"font-size: 20px;\">年轻人必须拼试(<span style=\"color: rgb(255, 0, 0);\">pins</span>)，不然和咸鱼有啥区别</span></span><br/></em></strong></p><p><strong><em><br/></em></strong></p><p><strong><em>&nbsp; &nbsp; &nbsp;<img alt=\"7.jpg\" src=\"../../static/userImages/20180607/1528379653111066127.jpg\" title=\"1528379653111066127.jpg\"/></em></strong></p><p><br/></p><p><strong><em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style=\"font-size: 20px;\">知识改变命运,技术改变未来</span><br/><br/>在此感谢曾经帮助的各位大神（导师），没有你们估计该作品很难完工</em></strong><br/></p><p><br/></p><p><br/><span style=\"color: rgb(255, 0, 0);\"><strong><em><span style=\"font-size: 36px;\"></span></em></strong></span><span style=\"color: rgb(255, 0, 0);\"><strong><em><span style=\"font-size: 36px;\"></span></em></strong></span><span style=\"font-size: 24px;\"></span></p>','pins1865','知识改变命运,技术改变未来','20180525052606.jpg');

UNLOCK TABLES;

/*Table structure for table `t_visit` */

DROP TABLE IF EXISTS `t_visit`;

CREATE TABLE `t_visit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `ip` varchar(15) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_visit` */

LOCK TABLES `t_visit` WRITE;

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
