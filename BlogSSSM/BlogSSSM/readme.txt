
开发技术：
后端开发框架SSSM(Spring,SpringMVC,Shiro,MyBatis),搜索引擎Lucene
前端开发框架bootstrap3.3.2  jquery-easyui-1.5.1

详细技术:
本项目使用Spring5+Springmvc5+Shiro+Mybatis3架构(SSSM)，
使用Mysql数据库存贮,使用Maven3管理项目，
使用MySQL/Lucene5混合模式全文搜索(混合模式仅登陆成功用户)，支持伪静态restful风格；
前台网页使用Bootstrap3框架；
后台管理使用EasyUI框架；
数据库连接池使用的是阿里巴巴的Druid；
在线编辑器使用百度的UEditor，
支持代码高亮特性；
使用gzip压缩缓存传输技术

项目特点；
本项目以文章为核心的轻量级文章管理系统，安装简单，结构简洁，
搜索支持多条件查询并高量显示，
可作为主站点的附加功能文章管理，
支持文章私有和公开，
可作为个人日志笔记，博客站点单独存在及其他文章管理功能等。


登陆地址/项目名/login.jsp或者userlogin.do 默认帐户:pins 密码:123456

演示站点
http://www.pins1865.com/blog